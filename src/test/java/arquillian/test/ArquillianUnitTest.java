/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arquillian.test;

import com.danielifg.jaxp.jaxp.parsers.dom.DOMParseXML;
import com.danielifg.jaxp.jaxp.parsers.sax.SAXParseXML;
import com.danielifg.jaxp.jaxp.parsers.stax.StAXParseXML;
import java.io.File;
import javax.inject.Inject;
import jaxpService.JAXP;
import static org.assertj.core.api.Assertions.assertThat;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;
import theaterServices.jaxp.Movie;

/**
 *
 * @author Daniel
 */
@RunWith(Arquillian.class)
public class ArquillianUnitTest {
    
     @Deployment
    public static WebArchive deploy() {

        // Use an alternative to the JUnit assert library called AssertJ
        // Need to reference MySQL driver as it is not part of GlassFish
        final File[] dependencies = Maven
                .resolver()
                .loadPomFromFile("pom.xml")
                .resolve(
                        "org.assertj:assertj-core").withoutTransitivity()
                .asFile();

        // For testing Arquillian prefers a resources.xml file over a
        // context.xml
        // Actual file name is resources-mysql-ds.xml in the test/resources
        // folder
        // The SQL script to create the database is also in this folder
        final WebArchive webArchive = ShrinkWrap.create(WebArchive.class, "test.war")               
                .addPackage(Movie.class.getPackage())
                .addPackage(DOMParseXML.class.getPackage())
                .addPackage(StAXParseXML.class.getPackage())
                .addPackage(SAXParseXML.class.getPackage())
                .addPackage(JAXP.class.getPackage())
                
                .addAsLibraries(dependencies);

        return webArchive;
    }
       @Inject
    private Movie movie;
    private JAXP jaxp;
    private DOMParseXML dom;
    
       @Test
       public void should_find_schedules_of_title_Krrish(){
           assertThat(jaxp.displayMovie("DOM", "cars").size()).isEqualTo(1);                
       }
       
        @Test
       public void should_find_studio_of_movie_Cars(){
           assertThat(dom.findMovieByTitle("cars").getStudio()).isEqualTo("Buena Vista");                
       }
}
