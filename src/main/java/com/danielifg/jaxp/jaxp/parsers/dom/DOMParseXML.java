package com.danielifg.jaxp.jaxp.parsers.dom;

import theaterServices.jaxp.Movie;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

/**
 *
 * @author Daniel
 *
 * CLass to Load the XML Document, Parse for the Queries by title, actor or
 * director, and loaded into the Movie been.
 *
 *
 */
public class DOMParseXML {

    private Document doc = null;

    private String XMLFILE = "movies.xml";
    private String xsdFile = null;
    private String message;

    public DOMParseXML() {

    }

    public boolean readDocument() {

        try {
            ClassLoader classLoader = getClass().getClassLoader();
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db;
            db = dbf.newDocumentBuilder();

            doc = db.parse(new File(classLoader.getResource(XMLFILE).getFile()));
            message = "true";
            return true;

        } catch (ParserConfigurationException ex) {
            message = "\nParserConfiguration error because ";
            message = ex.getMessage();
        } catch (SAXException ex) {
            message = "xml" + " cannot be parsed because ";
            message = ex.getMessage();
        } catch (IOException ex) {
            message = "\nI/O Exception because";
            message = ex.getMessage();
        }
        return false;
    }

    /**
     * Load the XML query results into the Movie bean.
     *
     * @param movie
     * @param eElement
     * @return
     *
     */
    public Movie loadBean(Element eElement) {
        Movie movie = new Movie();
        movie.setTitle(eElement.getElementsByTagName("title").item(0).getTextContent());
        movie.setRate(eElement.getElementsByTagName("rate").item(0).getTextContent());
        movie.getTimesList().add(eElement.getElementsByTagName("show_times").item(0).getTextContent());
        movie.setReleaseDate(eElement.getElementsByTagName("release_date").item(0).getTextContent());
        movie.setRunTime(eElement.getElementsByTagName("run_time").item(0).getTextContent());
        movie.setGenre(eElement.getElementsByTagName("genre").item(0).getTextContent());
        movie.getActorsList().add(eElement.getElementsByTagName("actors").item(0).getTextContent());
        movie.getDirectorsList().add(eElement.getElementsByTagName("directors").item(0).getTextContent());
        movie.getProducersList().add(eElement.getElementsByTagName("producers").item(0).getTextContent());
        movie.getWritersList().add(eElement.getElementsByTagName("writers").item(0).getTextContent());
        movie.setStudio(eElement.getElementsByTagName("studio").item(0).getTextContent());
        movie.setImage(eElement.getElementsByTagName("image").item(0).getTextContent());
        return movie;
    }

    public String getMessage() {
        return message;
    }

    /**
     * Loop through the Document to find query
     *
     * @param title
     *
     * @return if title in Movies file.
     */
    public Movie findMovieByTitle(String title) {
        readDocument();
        NodeList nList = doc.getElementsByTagName("movie");        
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            Element eElement = (Element) nNode;
            if (eElement.getElementsByTagName("title").item(0).getTextContent().equalsIgnoreCase(title)) {
                return loadBean(eElement);
            }
        }
        return null;
    }

    /**
     * Loop through the Document to find query
     *
     * @param actor
     * @return if Actor in Movies file.
     */
    public Movie findMovieByActorName(String actor) {
        readDocument();
        NodeList nList = doc.getElementsByTagName("movie");
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            Element eElement = (Element) nNode;

            NodeList actorsNode = (NodeList) eElement.getElementsByTagName("actors");
            for (int actorsIndex = 0; actorsIndex < actorsNode.getLength(); actorsIndex++) {
                for (int ii = 0; ii < actorsNode.item(actorsIndex).getChildNodes().getLength(); ii++) {
                    if (actorsNode.item(actorsIndex).getChildNodes().item(ii).getTextContent().equalsIgnoreCase(actor)) {
                        return loadBean(eElement);
                    }
                }
            }
        }
        return null;
    }

    /**
     * Loop through the Document to find query
     *
     * @param director
     * @return if director in Movies file.
     */
    public Movie findMovieByDirectorName(String director) {
        readDocument();
        NodeList nList = doc.getElementsByTagName("movie");

        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            Element eElement = (Element) nNode;

            NodeList directorsNode = (NodeList) eElement.getElementsByTagName("directors");
            for (int directorIndex = 0; directorIndex < directorsNode.getLength(); directorIndex++) {
                for (int ii = 0; ii < directorsNode.item(directorIndex).getChildNodes().getLength(); ii++) {
                    if (directorsNode.item(directorIndex).getChildNodes().item(ii).getTextContent().equalsIgnoreCase(director)) {
                        return loadBean(eElement);
                    }
                }
            }
        }

        return null;
    }

    public Movie findMovieByDOM(String query) {
        Movie m = new Movie();
        if (!(findMovieByTitle(query) == null)) {
            m = findMovieByTitle(query);
        } else if (!(findMovieByActorName(query) == null)) {
            m = findMovieByActorName(query);
        } else if (!(findMovieByDirectorName(query) == null)) {
            m = findMovieByDirectorName(query);
        }
        return m;
    }
}
