/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danielifg.jaxp.jaxp.parsers.stax;

import theaterServices.jaxp.Movie;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import jaxpService.JAXP;

/**
 *
 * @author Daniel
 *
 * Parsing XML with the XMLStreamReader Interface: Event Routine
 *
 */
public class StAXParseXML {

    private final String XMLFILE = "movies.xml";
    private  Movie movie;
    private Movie movie2;
    private String message;
    private final List<Movie> movieList = new ArrayList();

    public StAXParseXML() {

    }

    public void parseXmlEvents() throws XMLStreamException, FileNotFoundException {
        try {

            ClassLoader classLoader = getClass().getClassLoader();
            InputStream input = new FileInputStream(new File(classLoader.getResource(XMLFILE).getFile()));
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLStreamReader reader = factory.createXMLStreamReader(input);
            int eventType = reader.next();

            while (reader.hasNext()) {
                eventType = reader.next();

                if (eventType == XMLEvent.START_ELEMENT) {

                    String elementName = reader.getName().getLocalPart();

                    switch (elementName) {
                        case "movie":
                            movie2 = new Movie();
                            movieList.add(movie2);
                            break;
                        case "title":
                            movie2.setTitle(reader.getElementText());
                            break;
                        case "rate":
                            movie2.setRate(reader.getElementText());
                            break;
                        case "show_time":
                            movie2.getTimesList().add(reader.getElementText());
                            break;
                        case "release_date":
                            movie2.setReleaseDate(reader.getElementText());
                            break;
                        case "run_time":
                            movie2.setRunTime(reader.getElementText());
                            break;
                        case "genre":
                            movie2.setGenre(reader.getElementText());
                            break;
                        case "actor":
                            movie2.getActorsList().add(reader.getElementText());
                            break;
                        case "director":
                            movie2.getDirectorsList().add(reader.getElementText());
                            break;
                        case "producer":
                            movie2.getProducersList().add(reader.getElementText());
                            break;
                        case "writer":
                            movie2.getWritersList().add(reader.getElementText());
                            break;
                        case "studio":
                            movie2.setStudio(reader.getElementText());
                            break;
                        case "image":
                            movie2.setImage(reader.getElementText());
                            break;

                    }
                }
            }

        } catch (FactoryConfigurationError e) {
            message = ("FactoryConfigurationError" + e.getMessage() + "\n");
        } catch (XMLStreamException e) {
            message = ("XMLStreamException" + e.getMessage() + "\n");
        } catch (FileNotFoundException e) {
            message = ("IOException" + e.getMessage() + "\n");
        }
    }

    public String getMessage() {
        return this.message;
    }

    public Movie loadBean(int movieListIndex) {
            movie=new Movie();
        movie.setTitle(movieList.get(movieListIndex).getTitle());
        movie.setImage(movieList.get(movieListIndex).getImage());
        movie.setRate(movieList.get(movieListIndex).getRate());
        for (int ii = 0; ii < movieList.get(movieListIndex).getTimesList().size(); ii++) {
            movie.getTimesList().add(movieList.get(movieListIndex).getTimesList().get(ii));
        }
        movie.setReleaseDate(movieList.get(movieListIndex).getReleaseDate());
        movie.setRunTime(movieList.get(movieListIndex).getRunTime());
        movie.setGenre(movieList.get(movieListIndex).getGenre());
        for (int ii = 0; ii < movieList.get(movieListIndex).getActorsList().size(); ii++) {
            movie.getActorsList().add(movieList.get(movieListIndex).getActorsList().get(ii));
        }
        for (int ii = 0; ii < movieList.get(movieListIndex).getDirectorsList().size(); ii++) {
            movie.getDirectorsList().add(movieList.get(movieListIndex).getDirectorsList().get(ii));
        }
        for (int ii = 0; ii < movieList.get(movieListIndex).getProducersList().size(); ii++) {
            movie.getProducersList().add(movieList.get(movieListIndex).getProducersList().get(ii));
        }
        for (int ii = 0; ii < movieList.get(movieListIndex).getWritersList().size(); ii++) {
            movie.getWritersList().add(movieList.get(movieListIndex).getWritersList().get(ii));
        }
        movie.setStudio(movieList.get(movieListIndex).getStudio());
        return movie;
    }

    public Movie findMovieByTitle(String query) throws XMLStreamException, FileNotFoundException {
        parseXmlEvents();
        for (int i = 0; i < movieList.size(); i++) {
            if (movieList.get(i).getTitle().equalsIgnoreCase(query)) {
                return loadBean(i);
            }
        }
        return null;
    }

    public Movie findMovieByActorName(String query) throws XMLStreamException, FileNotFoundException {
        parseXmlEvents();
        for (int i = 0; i < movieList.size(); i++) {
            for (int actor = 0; actor < movieList.get(i).getActorsList().size(); actor++) {
                if (movieList.get(i).getActorsList().get(actor).equalsIgnoreCase(query)) {
                    return loadBean(i);
                }
            }
        }
        return null;
    }

    public Movie findMovieByDirectorName(String query) throws XMLStreamException, FileNotFoundException {
        parseXmlEvents();
        for (int i = 0; i < movieList.size(); i++) {
            for (int director = 0; director < movieList.get(i).getDirectorsList().size(); director++) {
                if (movieList.get(i).getDirectorsList().get(director).equalsIgnoreCase(query)) {
                    return loadBean(i);
                }
            }
        }
        return null;
    }

    public Movie findMovieByStAX(String query) throws XMLStreamException, FileNotFoundException {
        Movie m = new Movie();
        if (!(findMovieByTitle(query) == null)) {
            m = findMovieByTitle(query);
        } else if (!(findMovieByActorName(query) == null)) {
            m = findMovieByActorName(query);
        } else if (findMovieByDirectorName(query) == null) {
            m = findMovieByDirectorName(query);
        }
         Logger.getLogger(query);
        return m;
    }
}
