/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxpService;

import theaterServices.jaxp.Movie;
import com.danielifg.jaxp.jaxp.parsers.dom.DOMParseXML;
import com.danielifg.jaxp.jaxp.parsers.sax.SAXParseXML;
import com.danielifg.jaxp.jaxp.parsers.stax.StAXParseXML;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.stream.XMLStreamException;
import org.xml.sax.SAXException;

/**
 *
 * @author Daniel
 */
@WebService(serviceName = "JAXPservice")
public class JAXP {

    public JAXP() {
    }

    @WebMethod(operationName = "SaxParserService")
    public List<Movie> displayMovie(
            @WebParam(name = "engine") String strEngine,
            @WebParam(name = "query") String strQuery) {
        List<Movie> movieList = new ArrayList();
        try {
            String query = strQuery;
            String engine = strEngine;

            switch (engine) {
                case "DOM":
                    DOMParseXML dom = new DOMParseXML();
                    movieList.add(dom.findMovieByDOM(query));
                    break;
                case "SAX":
                    SAXParseXML sax = new SAXParseXML();
                    movieList.add(sax.findMovieBySAX(query));
                    break;
                case "StAX":
                    StAXParseXML stax = new StAXParseXML();
                    movieList.add(stax.findMovieByStAX(query));
                    break;
            }
        } catch (XMLStreamException | FileNotFoundException e) {
            movieList = null;
        }
        return movieList;
    }
}
